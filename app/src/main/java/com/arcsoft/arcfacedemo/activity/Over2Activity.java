package com.arcsoft.arcfacedemo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.arcsoft.arcfacedemo.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Over2Activity extends AppCompatActivity {

    @BindView(R.id.im_intnet)
    ImageView imIntnet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_over2);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.im_intnet)
    public void onViewClicked() {

        startActivity( new Intent(this,ScoreActivity.class));
    }
}
