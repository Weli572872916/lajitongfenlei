package com.arcsoft.arcfacedemo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.LinearLayout;

import com.arcsoft.arcfacedemo.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SeachActivity extends AppCompatActivity {

    @BindView(R.id.frameLayout)
    LinearLayout frameLayout;
    @BindView(R.id.bt_main)
    Button btMain;
    @BindView(R.id.home)
    NestedScrollView home;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seach);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.bt_main)
    public void onViewClicked() {
        startActivity( new Intent(this,Collection2Activity.class));
    }
}
