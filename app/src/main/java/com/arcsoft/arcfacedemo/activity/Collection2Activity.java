package com.arcsoft.arcfacedemo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.arcsoft.arcfacedemo.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Collection2Activity extends AppCompatActivity  {

    @BindView(R.id.frameLayout)
    FrameLayout frameLayout;
    @BindView(R.id.im_upload_photo)
    ImageView imUploadPhoto;
    @BindView(R.id.home)
    NestedScrollView home;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collection2);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.im_upload_photo)
    public void onViewClicked() {


            startActivity( new Intent(this,PhotoUpIngActivity.class));
    }
}
