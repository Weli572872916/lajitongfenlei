package com.arcsoft.arcfacedemo.bean;

public class Show {
    private String name;
    private int id;

    @Override
    public String toString() {
        return "Show{" +
                "name='" + name + '\'' +
                ", id=" + id +
                '}';
    }

    public Show() {
    }

    public Show(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
