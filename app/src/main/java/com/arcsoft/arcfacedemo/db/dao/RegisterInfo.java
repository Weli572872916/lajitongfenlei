package com.arcsoft.arcfacedemo.db.dao;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

@Entity
public class RegisterInfo {
    @Id(autoincrement = true)
    private Long id;
    private byte[] featureDate;
    private String name;
    private String phone;
    private String score;

    private int trackId;
    private String path;

    public RegisterInfo(byte[] featureDate, String name, String phone, String score, int trackId, String path) {
        this.featureDate = featureDate;
        this.name = name;
        this.phone = phone;
        this.score = score;
        this.trackId = trackId;
        this.path = path;
    }

    public RegisterInfo(byte[] featureDate, String name){
        this.featureDate = featureDate;
        this.name = name;
    }

    @Generated(hash = 1470244328)
    public RegisterInfo() {
    }

    @Generated(hash = 303501063)
    public RegisterInfo(Long id, byte[] featureDate, String name, String phone, String score, int trackId,
            String path) {
        this.id = id;
        this.featureDate = featureDate;
        this.name = name;
        this.phone = phone;
        this.score = score;
        this.trackId = trackId;
        this.path = path;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getFeatureDate() {
        return this.featureDate;
    }

    public void setFeatureDate(byte[] featureDate) {
        this.featureDate = featureDate;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getScore() {
        return this.score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public int getTrackId() {
        return this.trackId;
    }

    public void setTrackId(int trackId) {
        this.trackId = trackId;
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(String path) {
        this.path = path;
    }

}
