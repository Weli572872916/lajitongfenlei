package com.arcsoft.arcfacedemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.arcsoft.arcfacedemo.activity.Over2Activity;
import com.arcsoft.arcfacedemo.activity.PleseActivity;
import com.arcsoft.arcfacedemo.bean.Show;
import com.arcsoft.arcfacedemo.widget.FaceRectView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    List<Show> list = new ArrayList<>();
    RequestManager requestManager;
    BaseQuickAdapter<Show, BaseViewHolder> mAdapter;
    @BindView(R.id.texture_preview)
    TextureView texturePreview;
    @BindView(R.id.face_rect_view)
    FaceRectView faceRectView;
    @BindView(R.id.frameLayout)
    FrameLayout frameLayout;
    @BindView(R.id.home_recycler1)
    RecyclerView homeRecycler1;
    @BindView(R.id.bt_main)
    Button btMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


        int[] bodys1 = new int[]{R.drawable.img1, R.drawable.img2, R.drawable.img3, R.drawable.img4, R.drawable.img5, R.drawable.img6};


//        String[] strings1 = new String[]{"超级返", "9.9", "19.9", "优惠", "值得买"};
        String[] strings1 = new String[]{"厨余", "纸张", "塑料", "玻璃"
                , "金属", "其他"};
        list.clear();
        for (int i = 0; i < bodys1.length; i++) {
            Show show = new Show();
            show.setName(strings1[i]);
            show.setId(bodys1[i]);
            list.add(show);
        }
        requestManager = Glide.with(this);
        homeRecycler1.setLayoutManager(new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL));

        homeRecycler1.setAdapter(mAdapter = new BaseQuickAdapter<Show, BaseViewHolder>(R.layout.item_show, list) {

            @Override
            protected void convert(BaseViewHolder helper, Show item) {
                helper.setText(R.id.tv_show, item.getName());
                Glide.with(mContext).load(item.getId()).into((ImageView) helper.getView(R.id.im_show));

            }
        });
        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                startActivity( new Intent(MainActivity.this, PleseActivity.class));
            }
        });
    }

    @OnClick(R.id.bt_main)
    public void onViewClicked() {

    }
}
