package com.arcsoft.arcfacedemo.socket;


import io.netty.buffer.ByteBuf;

/**
 *
 * Created by LiuSaibao on 11/23/2016.
 */
public interface NettyListener {

    public final static byte STATUS_CONNECT_SUCCESS = 1;//连接成功

    public final static byte STATUS_CONNECT_CLOSED = 0;//关闭连接

    public final static byte STATUS_CONNECT_ERROR = 0;//连接失败


    /**
     * 当接收到系统消息
     */
    void onMessageResponse(Object msg);

    /**
     * 当连接状态发生变化时调用
     */
    public void onServiceStatusConnectChanged(int statusCode);
}