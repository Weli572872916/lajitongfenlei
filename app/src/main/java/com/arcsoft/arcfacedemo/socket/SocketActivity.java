package com.arcsoft.arcfacedemo.socket;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.arcsoft.arcfacedemo.R;
import com.arcsoft.arcfacedemo.util.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;

public class SocketActivity extends AppCompatActivity implements NettyListener{

    private Button btn_send;//发送
    private NettyClient nettyClient;//socket操作连接对象
    private final String TAG = "SocketActivity.class";
    TextView mTvRead;
    EditText med;
    @Override
protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_serial_port);
        initSocketTcp();//默认自动连接socket

        btn_send = findViewById(R.id.btn_send);//发送
        mTvRead= (TextView) findViewById(R.id.tv_receive);
        med=findViewById(R.id.et_send_content);
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!nettyClient.getConnectStatus()) {//获取连接状态，必须连接才能点。
                    Toast.makeText(SocketActivity.this, "先连接", Toast.LENGTH_SHORT).show();
                } else {

                    long l = System.currentTimeMillis();

                    /*
                    调用的发送。
                     */
                    nettyClient.sendMsgToServer(med.getText().toString(), new ChannelFutureListener() {
                        @Override
                        public void operationComplete(ChannelFuture channelFuture) throws Exception {
                            if (channelFuture.isSuccess()) {                //4
                                Log.d(TAG, "Write auth successful");
                            } else {
                                Log.d(TAG, "Write auth error");
                            }
                        }
                    });
                }
            }
        });
    }

    /*
     socket 端口号以及开始连接，配置接口监听
     */
    private void initSocketTcp() {
        nettyClient = new NettyClient(Constants.HOST, Constants.TCP_PORT);
        if (!nettyClient.getConnectStatus()) {
            nettyClient.setListener(SocketActivity.this);

            nettyClient.connect();
        } else {
            nettyClient.disconnect();
        }
    }


    /*
       回调客户端接收的信息  解析 数据流， 转换为string
       */
    @Override
    public void onMessageResponse(final Object msg) {
        ByteBuf result = (ByteBuf) msg;
        byte[] result1 = new byte[result.readableBytes()];
        result.readBytes(result1);
        result.release();
        final String ss = new String(result1);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Toast.makeText(SocketActivity.this, "接收成功" + ss, Toast.LENGTH_SHORT).show();
                mTvRead.append(ss);
            }
        });
    }

    //连接状态变化的时候 会走这
    @Override
    public void onServiceStatusConnectChanged(final int statusCode) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (statusCode == NettyListener.STATUS_CONNECT_SUCCESS) {
                    Log.e(TAG, "STATUS_CONNECT_SUCCESS:");
                    if (nettyClient.getConnectStatus()) {
                        Toast.makeText(SocketActivity.this, "连接成功", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.e(TAG, "onServiceStatusConnectChanged:" + statusCode);
                    if (!nettyClient.getConnectStatus()) {
                        Toast.makeText(SocketActivity.this, "网路不好，正在重连", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }
}
